import { INodeBlock } from "@tripetto/map";

/** Specifies the properties of the serialized block. */
export interface IFOTO extends INodeBlock {}
