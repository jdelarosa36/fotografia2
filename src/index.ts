/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import { ConditionCreateTemplates, Features, Forms, NodeBlock, PropertiesEditor, Slots, _, node } from "tripetto";
import { IFOTO } from "../types";

/** Assets */
import * as ICON from "../assets/icon.svg";

@node(PACKAGE_NAME, _("Foto", PACKAGE_VERSION), PACKAGE_VERSION)
export class FOTO extends NodeBlock<IFOTO> {
    /** Specifies an icon for the block. */
    public static readonly Icon = ICON;

    /** Map the block constructor to the type property. */
    public readonly Type = FOTO;

    /** Contains the slot. */
    public FOTO!: Slots.StringSlot;

    /** Invoked when slots for this block should be defined. */
    public OnSlots(): void {
        // Prepare your data slots here...
        this.FOTO = this.Slots.SelectOrCreate("static", Slots.StringSlot, "foto");
    }

    /** Invoked when the label of the node is changed. */
    public OnLabelChange(): void {
        this.FOTO.Placeholder = this.Node.Label;

        super.OnLabelChange();
    }

    /** Invoked when the properties editor is requested. */
    public OnProperties(properties: PropertiesEditor): void {
        /*properties.Features.Feature(
              "Feature example",
              new Forms.Form({
                Title: "Example",
                Controls: [
                  new Forms.Text("single").Label("This is a text input"),
                  new Forms.Checkbox("This is a checkbox")
                ]
              }));*/

        Features.Static.General(properties);

        const pName = Features.Name(this, properties);

        Features.Description(this, properties);
        Features.Placeholder(this, properties, pName);
        Features.Explanation(this, properties);

        Features.Static.Options(properties);
        Features.Required(this.FOTO, properties);
        Features.Visibility(this, properties);
        Features.Alias(this.FOTO, properties);
    }

    /** If your block supports conditions, return the create templates here. */
    public OnConditions(): ConditionCreateTemplates {
        return [];
    }
}
